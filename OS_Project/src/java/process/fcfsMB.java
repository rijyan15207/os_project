
package process;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "obj1")
@SessionScoped

public class fcfsMB 
{
    int value[], wt[], tat[], rt[];
    
    private int a, b, c, d;
    
    private double awt, atat, art;
    
    public fcfsMB()
    {
        value = new int[4];
        wt = new int[4];
        tat = new int[4];
        rt = new int[4];
    } 

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
        value[0] = this.a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
        value[1] = this.b;
    }

    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
        value[2] = this.c;
    }

    public int getD() {
        return d;
    }

    public void setD(int d) {
        this.d = d;
        value[3] = this.d;
    }

    public int[] getValue() {
        return value;
    }

    public void setValue(int[] value) {
        this.value = value;
    }

    public int[] getWt() {
        return wt;
    }

    public void setWt(int[] wt) {
        this.wt = wt;
    }

    public int[] getTat() {
        return tat;
    }

    public void setTat(int[] tat) {
        this.tat = tat;
    }

    public int[] getRt() {
        return rt;
    }

    public void setRt(int[] rt) {
        this.rt = rt;
    }

    public double getAwt() {
        return awt;
    }

    public void setAwt(double awt) {
        this.awt = awt;
    }

    public double getAtat() {
        return atat;
    }

    public void setAtat(double atat) {
        this.atat = atat;
    }

    public double getArt() {
        return art;
    }

    public void setArt(double art) {
        this.art = art;
    }
    
    
    
    
    public void process()
    {
        wt[0] = 0;
        rt[0] = 0;
        tat[0] = value[0];
        
        double maxwt = 0.0;
        double maxtat = tat[0];
        
        for(int i = 1; i < 4; i++)
        {
            wt[i] = value[i-1] +  wt[i-1];
            rt[i] = wt[i];
            maxwt = (double) maxwt + wt[i];
            
            tat[i] = wt[i] + value[i];
            maxtat = maxtat + tat[i];
        }
        
        awt = (double) maxwt / 4.0; 
        art = awt;
        atat = (double) maxtat / 4.0; 
    }
    
    
    public String exit()
    {
        a = b = c = d = 0;
        awt = art = atat = 0.0;
        for(int i = 0; i < 4; i++)
        {
            value[i] = wt[i] = tat[i] = rt[i] = 0;
        } 
        return "choice.xhtml";
    }
}

