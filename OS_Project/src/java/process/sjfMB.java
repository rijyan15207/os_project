
package process;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "obj2")
@SessionScoped

public class sjfMB 
{
    int value[], wt[], tat[], rt[], p[];
    
    private int a, b, c, d;
    
    private double awt, atat, art;
    
    public sjfMB() 
    {
        value = new int[4];
        wt = new int[4];
        tat = new int[4];
        rt = new int[4];
        p = new int[4];
    } 
    
    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
        value[0] = this.a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
        value[1] = this.b;
    }

    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
        value[2] = this.c;
    }

    public int getD() {
        return d;
    }

    public void setD(int d) {
        this.d = d;
        value[3] = this.d;
    }

    public int[] getValue() {
        return value;
    }

    public void setValue(int[] value) {
        this.value = value;
    }

    public int[] getWt() {
        return wt;
    }

    public void setWt(int[] wt) {
        this.wt = wt;
    }

    public int[] getTat() {
        return tat;
    }

    public void setTat(int[] tat) {
        this.tat = tat;
    }

    public int[] getRt() {
        return rt;
    }

    public void setRt(int[] rt) {
        this.rt = rt;
    }

    public double getAwt() {
        return awt;
    }

    public void setAwt(double awt) {
        this.awt = awt;
    }

    public double getAtat() {
        return atat;
    }

    public void setAtat(double atat) {
        this.atat = atat;
    }

    public double getArt() {
        return art;
    }

    public void setArt(double art) {
        this.art = art;
    }

    public int[] getP() {
        return p;
    }

    public void setP(int[] p) {
        this.p = p;
    }
    
    
    
    
    public void process()
    {
        p[0] = 1; 
        p[1] = 2; 
        p[2] = 3; 
        p[3] = 4; 
        
        
        // Bubble Sort
        int temp;
        for (int i = 0; i < 4; i++) 
        { 
            for (int j = i+1; j < 4; j++) 
            { 
                if(value[i] > value[j]) 
                { 
                    temp = value[i]; 
                    value[i] = value[j]; 
                    value[j] = temp; 
                    
                    temp = p[i]; 
                    p[i] = p[j]; 
                    p[j] = temp; 
                } 
            } 
        } 
        
        
        
        // Execution 
        
        wt[0] = 0; 
        rt[0] = 0; 
        
        tat[0] = value[0]; 
        
        double maxwt = 0.0; 
        double maxtat = tat[0]; 
        
        for(int i = 1; i < 4; i++) 
        {
            wt[i] = value[i-1] +  wt[i-1]; 
            rt[i] = wt[i]; 
            maxwt = (double) maxwt + wt[i]; 
            
            tat[i] = wt[i] + value[i]; 
            maxtat = maxtat + tat[i]; 
        } 
        
        awt = (double) maxwt / 4.0; 
        art = awt; 
        atat = (double) maxtat / 4.0; 
    } 
    
    
    public String exit()
    {
        a = b = c = d = 0;
        awt = art = atat = 0.0;
        for(int i = 0; i < 4; i++)
        {
            value[i] = wt[i] = tat[i] = rt[i] = p[i] = 0;
        } 
        return "choice.xhtml";
    }
}

